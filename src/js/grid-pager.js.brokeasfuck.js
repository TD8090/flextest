!function ($) {
    "use strict"; // jshint ;_;
    var GridPagerInit = function (element, options) {
        this.init(element, options);
    };
    GridPagerInit.prototype = {
        constructor: GridPagerInit
        , init: function (element, options) {
            var controls = ''
                , $container = $(element)
                , $cols, $total, i, ii;

            this.$container = $container;
            this.$options = this.getOptions(options);
            this.$numItems = $container.find(this.$options.cls);//number of classname els found within container
            this.$pg = 3;
            if(this.$numItems > this.$options.tpp){//if num of griditems > total per page setting
                ii = -1+this.$options.tpp;
                for(i=this.$numItems;i>ii;i--){// i is 43, loop counting down until TPP-1 (3) is reached (42x)
                    this.$container.find(this.$options.cls+':eq('+i+')').hide();
                }
                this.initNavs(this.$pg);
                this.navcontainer = $('#grid-pager-nav').children('ul');
                this.navlinks = this.navcontainer.find('li a');
            }
            this.listen(this.navlinks);
        }

        , initNavs: function(){//_________________SETUP THE CONTROLS_________________
            var navcontainer = $("<nav>", {id: 'grid-pager-nav', class: 'text-center'});
            navcontainer.after(this.$container);

            var numpgs = Math.ceil(this.$numItems/this.$options.tpp)
                , controls = '<nav class="text-center" id="grid-pager-nav"><ul class="pagination">'
                , range, i, ii, btn = [], makeNavId = '', elli = "...";
            for(i=1;i<=numpgs;i++){
                controls += '<li><a href="javascript:void(0);" data-go-to="'+i+'">'+(i)+'</a></li>';
            }
            controls += '</ul></nav>';
            this.$container.after(controls);
            for(ii=1;ii<=9;ii++){//make 9 buttons with IDs, reference in array
                makeNavId = 'btn'+ii;
                btn[ii] = $("<li>", {id: makeNavId, class: "pagbtn"});
                btn[ii].appendTo(this.navcontainer);
            }
        }
        , updateNavs: function(){//_________________CHANGE THE CONTROLS_________________
            this.navcontainer.empty();

            var numpgs = Math.ceil(this.$numItems/this.$options.tpp)
                , controls = '<nav class="text-center" id="#grid-pager-nav"><ul class="pagination">'
                , range, i;
            for(i=0;i<numpgs;i++){
                controls += '<li><a href="javascript:void(0);" data-go-to="'+i+'">'+(i+1)+'</a></li>';
            }
            controls += '</ul></nav>';
            controls.appendTo(this.navcontainer);
        }



        //>>>>>>>>>>>>>> ON CLICK - USE [DATA-GO-TO VAL] TO SHOW/HIDE GRID ITEMS <<<<<<<<<<<<<<
        //...AND REBUILD NAVS (call initNavs) BASED ON [DATA-GO-TO VAL],
        //   ......numitems($numItems) and numpages based on tpp
        , showPage: function (e){
            //set var pg to the clicked elmts datagoto value
            var pg = $(e.currentTarget).data('go-to')                   //clicked control with datagoto7, pg=7
                , startshow = parseInt(pg*this.$options.tpp)            // 7*tpp=start  > start=24
                , endshow = parseInt((startshow+this.$options.tpp)-1)       // 24+tpp-1     >   end=27
                , i;

            for(i=0;i<this.$numItems;i++){//count up to num of total items with 'cls' setting (.grid-pager)
                if(i < startshow || i > endshow){//total is 43, count up, when i>=24 enter next test, else just hide
                    this.$container.find(this.$options.cls+':eq('+i+')').hide();
                }
                else {//show the .grid-pager with index of i
                    this.$container.find(this.$options.cls+':eq('+i+')').show();
                }
            }
            //this.updateNavs(this.pg);
        }
        , listen: function(navlinks){
            //"pager" is $("grid-pager-nav li a") elmts by default (all a elmts in li elmts in id)
            navlinks.on('click',  $.proxy(this.showPage, this));//onClick call showPage passing "e" arg
        }
        , getOptions: function (options) {
            options = $.extend({}, $.fn['gridPager'].defaults, options, this.$container.data());
            return options;
        }
        , sel: function(button){
            $(".pagbtn").css('backgroundColor', 'teal' );//reset all colors
            $("#btn"+button ).css('backgroundColor', 'blue' );//set
        }
        , nums_bot: function(){//b8 ellipsis, 1-7 low num range
            var i;this.btn[8].textContent = '...';
            for(i=1;i<=7;i++){               //count up from min
                this.btn[i].textContent = i; //set text within #btns
                this.btn[i].attr( 'data-go-to', i );
            }
        }
        , nums_mid: function(curpg){//both ellipsis, dynamic nums

            this.btn[2].textContent = this.elli ; this.btn[2].data('goTo', ''   );
            this.btn[3].textContent = curpg-2 ; this.btn[3].data('goTo', curpg-2 );
            this.btn[4].textContent = curpg-1 ; this.btn[4].data('goTo', curpg-1 );
            this.btn[5].textContent = curpg   ; this.btn[5].data('goTo', curpg   );
            this.btn[6].textContent = curpg+1 ; this.btn[6].data('goTo', curpg+1 );
            this.btn[7].textContent = curpg+2 ; this.btn[7].data('goTo', curpg+2 );
            this.btn[8].textContent = this.elli ; this.btn[8].data('goTo', ''   );
        }
        , nums_top: function(){//b2 ellipsis, 9-3 high num range
            var i;this.btn[2].textContent = '...';
            for(i=pgmax; i>=pgmax-6; i--){    //count down from max
                this.btn[i].textContent = i;  //set text within #btns
                this.btn[i].attr( 'data-go-to', i );
            }
        }





    };//END:GridPagerInit.prototype

    /* GridPagerInit PLUGIN DEFINITION
     * ======================== */
    $.fn.gridPager = function (option) {
        return this.each(function () {
            var $this = $(this)
                , data = $this.data('gridPager')
                , options = typeof option == 'object' && option;
            if (!data) $this.data('gridPager', (data = new GridPagerInit(this, options)));
            if (typeof option == 'string') data[option]()
        })
    };

    $.fn.gridPager.Constructor = GridPagerInit;
    /* GridPagerInit DATA-API
     * =============== */
    $.fn.gridPager.defaults = {
        cls: '.grid-pager',
        tpp: 12
    }

}(window.jQuery);