<!DOCTYPE html><html lang="en"><head><meta charset="utf-8"><title>VI-xmlParser</title>
    <link href="src/bootstrap/css/bootstrap.darkly.min.css" rel="stylesheet">
</head>
<body>
<div class="container-fluid">
    <div id="Header" class="row">
        <div class="" id="viheader" style="-moz-user-select: none; -webkit-user-select: none;
         -ms-user-select:none; user-select:none;-o-user-select:none;"
             unselectable="on" onselectstart="return false;" onmousedown="return false;">
            Vi XML Parser
        </div>
    </div>
    <div id="Input" class="row">
        <form method='get'>
            <div class="col-xs-12">
                <div class="input-group">
                    <div class="input-group-btn">
                        <button tabindex="-1" id="readXML" type="submit" class="btn btn-primary" name="readXML">Read XML</button>
                        <button tabindex="-1" data-toggle="dropdown" class="btn btn-default dropdown-toggle" type="button">
                            <span class="caret"></span>
                            <span class="sr-only">Toggle Dropdown</span>
                        </button>
                        <ul class="dropdown-menu" id="locationselector">
                            <li><button type="button" class="locselect btn btn-primary btn-xs btn-block f-nova"
                                        value="http://campusdining.compass-usa.com/uncg/Pages/SignageXML.aspx?location=Fountain%20View">
                                    Fountain View</button></li>
                            <li><button type="button" class="locselect btn btn-primary btn-xs btn-block f-nova"
                                        value="http://morrisondining.compass-usa.com/sentara/Pages/SignageXML.aspx?location=Mountain%20View%20Cafe">
                                    Mountain View Cafe</button></li>
                        <?php/*________________________________*/


                            while($i= $urls.length){
                            $thisurl = ;
                            $button_saveloc = <<<EOT
                            <li><button type="button" class="locselect btn btn-primary btn-xs btn-block f-nova"
                                        value="$thisurl">
                                    Mountain View Cafe</button></li>
EOT;
                            echo $button_saveloc;
                            $i++;
                            }
                        ?>
                            <li class="divider"></li>
                        </ul>
                    </div>
                    <input id="inputarea" type="text" class="form-control" name="pastedURL"
                           onchange="checkinputarea();" onkeyup="this.onchange();"
                           onpaste="this.onchange();" oninput="this.onchange();"
                           placeholder="...XML address..." />
                </div>
            </div>
        </form>
    </div>
    <div id="NinjaOutput" class="row">
    <?php
if(isset($_GET['readXML'])) {
    $simpxml = simplexml_load_file($_GET['pastedURL'], 'SimpleXMLElement', LIBXML_NOWARNING);
    //$xml = simplexml_load_file('http://campusdining.compass-usa.com/uncg/Pages/SignageXML.aspx?location=Fountain%20View');
    if (!$simpxml) {
      echo "<div class='col-xs-8 col-xs-offset-2 text-center alert alert-info' style='opacity:.8;' role='alert'>...Camelot! ...Camelot~!  ...CAMELOT!!<br/>(it's only an error...)</div>";
    } else {
    $pasted_url = $_GET['pastedURL'];
$button_saveloc = <<<EOT
    <div class='col-sm-10 col-sm-offset-1'>
        <div id='successlinkdiv'><a href='$pasted_url' id='successlink'>$pasted_url</a></div>
        <button class="btn btn-success btn-xs" id="locsave" role="button">
            <span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
            Add
        </button>
        <span class="panel panel-success" style="display:none; color:#00ffbd;" id="locsave_response"></span>
    </div>
EOT;
        echo $button_saveloc;
        $domx = new DOMDocument("1.0");
        $domx->preserveWhiteSpace = false;
        $domx->formatOutput = true;
        $domx->loadXML($simpxml->asXML()); //domx is now xml object
        $domxText = print_r($domx, true);
        $formatted =  "<pre id='pretag' class='hide'>" . htmlentities($domx->saveXML()) . "</pre>";
        echo $formatted;
    }
}
    ?>
    </div>
    <div id="Tree">

    </div>
    <div id="JsOutput" class="row">
    </div>

<!--

        <table id="radtab-fbasis" class="radiotable" data-toggle="buttons">
            <tr><td>1</td><td>2</td><td>3</td><td>4</td><td>5</td></tr>
            <tr><td>6</td><td>7</td><td>8</td><td>9</td><td>10</td></tr>
            <tr><td>11</td><td>12</td><td>13</td><td>14</td><td>15</td></tr>
            <tr><td>16</td><td>17</td><td>18</td><td>19</td><td>20</td></tr>
            <tr><td>21</td><td>22</td><td>23</td><td>24</td><td>25</td></tr>
            <tr><td>26</td><td>27</td><td>28</td><td>29</td><td>30</td></tr>
            <tr><td>35</td><td>40</td><td>45</td><td>50</td><td>60</td></tr>
            <tr><td colspan="5">auto</td></tr>
            <tr><td colspan="5">content</td></tr>
        </table>
-->
</div>
<!--/////end:(BODY)/////-->

<br/><br/><br/>

<!-- ==========JS=========== -->
<!--##JQUERY-->
<script src="src/js/jquery-1.11.3.min.js" type="text/javascript"></script>
<!--##BOOTSTRAP_3.3-->
<script src="src/bootstrap/js/bootstrap.min.js"></script>
<!--##USER(JS)-->
<script src="src/js/script.js"></script>
<script>


    .html(function(){
        var fbasis='';
        for(j=1;j<=5;j++){fbasis+='<tr><td></td><td></td><td></td><td></td><td></td></tr>';}
        for(j=1;j<=2;j++){fbasis+='<tr><td colspan="5"></td></tr>';}
        return fbasis;
    })
        .find("td").each(function(index, elmt){
            var num = index+1;
            if(num<=basismax){$(elmt).text(num)}
            else if(num == basismax+1 ){$(elmt).text("auto")}
            else if(num == basismax+2 ){$(elmt).text("content")}
        });
    var style = document.createElement('style');
    style.type = 'text/css';
    style.innerHTML = '.cssClass { color: #F00; }';
    document.getElementsByTagName('head')[0].appendChild(style);

    document.getElementById('someElementId').className = 'cssClass';


    /*
     radtabfbasis.find("td").on("click", function(){
     if(last_basis){last_basis.css({'background-color': '#5f5'});}
     $(this).css({'background-color': 'grey'});
     last_basis = $(this);
     var tval = $(this).text();
     $('.cell').css({'flex-basis': tval+'%'});
     });
     */


/*
    <input type="button" onclick="toggleZoomScreen()" value="100% Zoom"/>
*/
    function toggleZoomScreen() {
        $('body').css({
            'zoom':'0',
            '-moz-transform': 'scale(0)',
            '-moz-transform-origin': '0 0'
        });
        console.log("helloooooo");
    }




    var stylz = ''
            ,fp_dir__row      ='flex-direction: row;'
            ,fp_dir_rowrev    ='flex-direction: row-reverse;'
            ,fp_dir_col       ='flex-direction: column;'
            ,fp_dir_colrev    ='flex-direction: column-reverse;'
            ,fp_j__flxstart   ='justify-content: flex-start;'
            ,fp_j_flxend      ='justify-content: flex-end;'
            ,fp_j_center      ='justify-content: center;'
            ,fp_j_spcbtwn     ='justify-content: space-between;'
            ,fp_j_spcarnd     ='justify-content: space-around;'
            ,fp_wrap__nowrap  ='flex-wrap: nowrap;'
            ,fp_wrap_wrap     ='flex-wrap: wrap;'
            ,fp_wrap_wraprev  ='flex-wrap: wrap-reverse;'
            ,fp_ac__stretch   ='align-content: stretch;'
            ,fp_ac_flexstart  ='align-content: flex-start;'
            ,fp_ac_flexend    ='align-content: flex-end;'
            ,fp_ac_center     ='align-content: center;'
            ,fp_ac_spcbetween ='align-content: space-between;'
            ,fp_ac_spcaround  ='align-content: space-around;'
            ,fp_ai__stretch   ='align-items: stretch;'
            ,fp_ai_flexstart  ='align-items: flex-start;'
            ,fp_ai_flexend    ='align-items: flex-end;'
            ,fp_ai_center     ='align-items: center;'
            ,fp_ai_baseline   ='align-items: baseline;'
            ,fc_as__auto      ='align-self: auto;'
            ,fc_as_flexstart  ='align-self: flex-start;'
            ,fc_as_flexend    ='align-self: flex-end;'
            ,fc_as_center     ='align-self: center;'
            ,fc_as_baseline   ='align-self: baseline;'
            ,fc_grow__0       ='flex-grow: 0;'
            ,fc_grow_1        ='flex-grow: 1;'
            ,fc_shrink__1     ='flex-shrink: 1;'
            ,fc_shrink_0      ='flex-shrink: 0;'
            ,fc_basis__auto   ='flex-basis: auto;'
            ,fc_basis_cont    ='flex-basis: content;'
            ;
</script>
</body>
</html>