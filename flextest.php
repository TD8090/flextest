<!DOCTYPE html><html><head><meta charset="utf-8"><title>VI-FlexTest</title><meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <!-- ==========CSS=========== -->
    <link href="src/bootstrap/css/bootstrap.darkly.min.css" rel="stylesheet">
    <link href="flextest.css" rel="stylesheet">
    <!-- ==========JS=========== -->
    <script src="src/js/jquery-1.11.3.min.js" type="text/javascript"></script>
    <script src="src/bootstrap-3.3.5-dist/js/bootstrap.min.js"></script>
    <style type="text/css" id="sgen"></style>
</head>
<body><div id="Header"><div id="viheader" unselectable="on" onselectstart="return false;" onmousedown="return false;">Vi FlexTester</div></div>


<div id="changeling-panel">
    <div class="btn-toolbar btn-group-sm panel panel-warning">
        <div id="flex-direction" class="btn-group-vertical btn-group-xs" data-toggle="buttons">
            <h4>flex-direction</h4>
            <label class="btn btn-warning"><input type="radio" name="options" value="row">^row</label>
            <label class="btn btn-warning"><input type="radio" name="options" value="row-reverse">row-reverse</label>
            <label class="btn btn-warning"><input type="radio" name="options" value="column">column</label>
            <label class="btn btn-warning"><input type="radio" name="options" value="column-reverse">column-reverse</label>
        </div>
        <div id="flex-wrap" class="btn-group-vertical btn-group-xs" data-toggle="buttons">
            <h4>flex-wrap</h4>
            <label class="btn btn-warning"><input type="radio" name="options" value="nowrap">^nowrap</label>
            <label class="btn btn-warning"><input type="radio" name="options" value="wrap">wrap</label>
            <label class="btn btn-warning"><input type="radio" name="options" value="wrap-reverse">wrap-reverse</label>
        </div>
    </div>
    <div class="btn-toolbar btn-group-sm panel panel-warning">
        <div id="justify-content" class="btn-group-vertical btn-group-xs" data-toggle="buttons">
            <h4>justify-content</h4>
            <label class="btn btn-warning"><input type="radio" name="options" value="flex-start">^flex-start</label>
            <label class="btn btn-warning"><input type="radio" name="options" value="flex-end">flex-end </label>
            <label class="btn btn-warning"><input type="radio" name="options" value="center">center</label>
            <label class="btn btn-warning"><input type="radio" name="options" value="space-between">space-between</label>
            <label class="btn btn-warning"><input type="radio" name="options" value="space-around">space-around</label>
        </div>
    </div>
    <div class="btn-toolbar btn-group-sm panel panel-warning">
        <div id="align-content" class="btn-group-vertical btn-group-xs" data-toggle="buttons">
            <h4>align-content</h4>
            <label class="btn btn-warning"><input type="radio" name="options" value="stretch">^stretch</label>
            <label class="btn btn-warning"><input type="radio" name="options" value="flex-start">flex-start</label>
            <label class="btn btn-warning"><input type="radio" name="options" value="flex-end">flex-end</label>
            <label class="btn btn-warning"><input type="radio" name="options" value="center">center</label>
            <label class="btn btn-warning"><input type="radio" name="options" value="space-between">space-between</label>
            <label class="btn btn-warning"><input type="radio" name="options" value="space-around">space-around</label>
        </div>
    </div>
    <div class="btn-toolbar btn-group-sm panel panel-warning">
        <div id="align-items" class="btn-group-vertical btn-group-xs" data-toggle="buttons">
            <h4>align-items</h4>
            <label class="btn btn-warning"><input type="radio" name="options" value="stretch">^stretch</label>
            <label class="btn btn-warning"><input type="radio" name="options" value="flex-start">flex-start</label>
            <label class="btn btn-warning"><input type="radio" name="options" value="flex-end">flex-end</label>
            <label class="btn btn-warning"><input type="radio" name="options" value="center">center</label>
            <label class="btn btn-warning"><input type="radio" name="options" value="baseline">baseline</label>
        </div>
    </div>
    <div class="btn-toolbar btn-group-sm panel panel-success">
        <div id="items" class="btn-group btn-group-xs" data-toggle="buttons">
            <h4>items</h4>
            <button class="btn btn-xs btn-primary w50" id="removecell">ITEM-</button>
            <button class="btn btn-xs btn-primary w50" id="addcell">ITEM+</button>
        </div>
        <div id="height" class="btn-group btn-group-xs" data-toggle="buttons">
            <h4>height</h4>
            <label class="btn btn-warning w25"><input type="radio" name="options" value="auto">auto</label>
            <label class="btn btn-warning w25"><input type="radio" name="options" value="100%">100%</label>
            <label class="btn btn-warning w25"><input type="radio" name="options" value="initial">init</label>
            <label class="btn btn-warning w25"><input type="radio" name="options" value="inherit">inher</label>
        </div>
        <div id="width" class="btn-group btn-group-xs" data-toggle="buttons">
            <h4>width</h4>
            <label class="btn btn-warning w25"><input type="radio" name="options" value="auto">auto</label>
            <label class="btn btn-warning w25"><input type="radio" name="options" value="100%">100%</label>
            <label class="btn btn-warning w25"><input type="radio" name="options" value="initial">init</label>
            <label class="btn btn-warning w25"><input type="radio" name="options" value="inherit">inher</label>
        </div>

        <div id="max-height" class="btn-group btn-group-xs" data-toggle="buttons">
            <h4>max-height</h4>
            <label class="btn btn-warning w33"><input type="radio" name="options" value="none">none</label>
            <label class="btn btn-warning w33"><input type="radio" name="options" value="100%">100%</label>
            <label class="btn btn-warning w33"><input type="radio" name="options" value="inherit">inher</label>
        </div>

        <div id="max-width" class="btn-group btn-group-xs" data-toggle="buttons">
            <h4>max-width</h4>
            <label class="btn btn-warning w33"><input type="radio" name="options" value="none">none</label>
            <label class="btn btn-warning w33"><input type="radio" name="options" value="100%">100%</label>
            <label class="btn btn-warning w33"><input type="radio" name="options" value="inherit">inher</label>
        </div>


    </div>
    <div class="btn-toolbar btn-group-sm panel panel-success">
        <div id="align-self" class="btn-group-vertical btn-group-xs" data-toggle="buttons">
            <h4>align-self</h4>
            <label class="btn btn-warning"><input type="radio" name="options" value="auto">^auto</label>
            <label class="btn btn-warning"><input type="radio" name="options" value="flex-start">flex-start</label>
            <label class="btn btn-warning"><input type="radio" name="options" value="flex-end">flex-end</label>
            <label class="btn btn-warning"><input type="radio" name="options" value="center">center</label>
            <label class="btn btn-warning"><input type="radio" name="options" value="baseline">baseline</label>
        </div>
    </div>
    <div class="btn-toolbar btn-group-sm panel panel-success">
        <div id="flex-grow" class="btn-group btn-group-xs" data-toggle="buttons">
            <h4>flex-grow</h4>
            <label class="btn btn-warning w50"><input type="radio" name="options" value="0">^0</label>
            <label class="btn btn-warning w50"><input type="radio" name="options" value="1">1</label>
        </div>
        <div id="flex-shrink" class="btn-group btn-group-xs" data-toggle="buttons">
            <h4>flex-shrink</h4>
            <label class="btn btn-warning w50"><input type="radio" name="options" value="1">^1</label>
            <label class="btn btn-warning w50"><input type="radio" name="options" value="0">0</label>
        </div>
    </div>
    <div class="btn-toolbar btn-group-sm panel panel-success text-center">
        <h4>flex-basis</h4>
        <button id="poptip_flexbasis" type="button" class="btn-poptip" data-container="body"
                data-toggle="popover" data-placement="bottom" data-trigger="focus" >?</button>

        <div id="radtabfbasis" class="btn-group-xs" data-toggle="buttons">
        </div>

    </div>
    <div class="btn-toolbar btn-group-sm panel panel-success text-center">
        <div id="items" class="btn-group btn-group-xs" data-toggle="buttons">
            <h4>spec-ops</h4>
            <button class="btn btn-xs btn-primary" id="matchfb">Match Flex-basis</button>
        </div>
    </div>
</div>

<div id="sandbox">
</div>
<div id="styleswap"></div>
<script>
    var a,b,c,d,e,f,g,h,i,j,k,prop,val, last_basis, tval, initimgs = '',ifb = '',getstyle = '', restyle='',
            sandbox=$("#sandbox"), cells=$('.cell'), addcell=$("#addcell"), radtabfbasis = $("#radtabfbasis"),rmvcell=$("#removecell");
//    var cell = function(){return "<div class='cell'><img src='http://lorempixel.com/300/500?random="+randInt(1, 100)+"' /></div>"};
    var cell = function(){return "<div class='cell'><img src='src/img/testbottles/"+randInt(1, 161)+".png' /></div>"};
    sandbox.append(function (){for(a=1;a<10;a++){initimgs += cell();}return initimgs;});
    radtabfbasis.append(function (){
        for(b=1;b<=50;b++){
            if(b==1){ifb += '<label class="btn btn-success"><input type="radio" name="options" value="'+b+'%">'+b+'</label>';}
            else if(b>1){ifb += '<label class="btn btn-success"><input type="radio" name="options" value="'+b+'%">'+b+'</label>';}
        }
        for(c=50;c<=500;c+=50){
            ifb += '<label class="btn btn-primary"  style="font-size:8px;"><input type="radio" name="options" value="'+c+'px">'+c+'</label>';
        }
        ifb += '<label class="btn btn-success"><input type="radio" name="options" value="auto">auto</label>';
        ifb += '<label class="btn btn-success"><input type="radio" name="options" value="content">content</label>';
        return ifb;
    });
    $('#flex-direction')  .find('label').has('input[value="row"]')          .addClass('active');
    $('#flex-wrap')       .find('label').has('input[value="wrap"]')         .addClass('active');
    $('#justify-content') .find('label').has('input[value="space-around"]') .addClass('active');
    $('#align-content')   .find('label').has('input[value="stretch"]')      .addClass('active');
    $('#align-items')     .find('label').has('input[value="stretch"]')      .addClass('active');
    $('#height')          .find('label').has('input[value="auto"]')         .addClass('active');
    $('#width')           .find('label').has('input[value="auto"]')         .addClass('active');
    $('#max-height')      .find('label').has('input[value="100%"]')         .addClass('active');
    $('#max-width')       .find('label').has('input[value="100%"]')         .addClass('active');
    $('#align-self')      .find('label').has('input[value="auto"]')         .addClass('active');
    $('#flex-grow')       .find('label').has('input[value="0"]')            .addClass('active');
    $('#flex-shrink')     .find('label').has('input[value="1"]')            .addClass('active');
    radtabfbasis          .find('label').has('input[value="16%"]')          .addClass('active');

    addcell.on("click", function(){sandbox.append(cell());});
    rmvcell.on("click", function(){sandbox.find(".cell:last-child").remove();});
    console.log(cells.length);

    var styleTag = document.getElementById('sgen');
    var newstyles='';

    var styleStuff = function(){
        //grab all settings from active tags

        var fdir = $('#flex-direction').find('label.active>input').val();
        var fwra = $('#flex-wrap').find('label.active>input').val();
        var fjst = $('#justify-content').find('label.active>input').val();
        var falc = $('#align-content').find('label.active>input').val();
        var fali = $('#align-items').find('label.active>input').val();
        var fals = $('#align-self').find('label.active>input').val();
        var fgro = $('#flex-grow').find('label.active>input').val();
        var fshr = $('#flex-shrink').find('label.active>input').val();
        var fbas = $('#radtabfbasis').find('label.active>input').val();
        var ihei = $('#height').find('label.active>input').val();
        var iwid = $('#width').find('label.active>input').val();
        var imxh = $('#max-height').find('label.active>input').val();
        var imxw = $('#max-width').find('label.active>input').val();

//        console.log("dir:"+fdir,"//wr:"+fwra,"//jst:"+fjst,"//a-c:"+falc,"//a-i:"+fali);
//        console.log("//a-s:"+fals,"//gro:"+fgro,"//shr:"+fshr,"//bas:"+fbas);
        newstyles='#sandbox{\n';
        newstyles+='    flex-direction: '+fdir+';\n';
        newstyles+='    flex-wrap: '+fwra+';\n';
        newstyles+='    justify-content: '+fjst+';\n';
        newstyles+='    align-content: '+falc+';\n';
        newstyles+='    align-items: '+fali+';\n';
        newstyles+='}\n';
        newstyles+='.cell{\n';
        newstyles+='    align-self: '+fals+';\n';
        newstyles+='    flex-grow: '+fgro+';\n';
        newstyles+='    flex-shrink: '+fshr+';\n';
        newstyles+='    flex-basis:'+fbas+';\n';
        newstyles+='}\n';
        newstyles+='.cell img{\n';
        newstyles+='    height: '+ihei+';\n';
        newstyles+='    width: '+iwid+';\n';
        newstyles+='    max-height: '+imxh+';\n';
        newstyles+='    max-width: '+imxw+';\n';
        newstyles+='}';
        console.log(newstyles);
        styleTag.innerHTML = newstyles;
    };
    styleStuff();
    $('div[data-toggle="buttons"]').on('change', styleStuff);

    //style tag... detect onchange and read the active elements for their
    $('#matchfb').on("click", function(){
        getstyle = styleTag.html;
        console.log(getstyle);
        var curwid = $('#sandbox').first('img').offsetWidth;
        console.log(curwid);
        restyle = getstyle.replace(/flex\-basis:(.*)(?=;)/, curwid.toString());
        console.log(restyle);
        styleTag.innerHTML = restyle;
    });

$('#poptip_flexbasis').data('content', function(){
   return "Vivamus sagittis lacus vel augue laoreet rutrum faucibus.";
});
/*r_ord0='order:0;',
 r_ord1='order:1;',*/
    //====================
    //=========flex-parent
    //  flex-direction              //  flex-wrap         //  align-content
    //    ^row                      //    ^nowrap         //    ^stretch            //========flex-children
    //    row-reverse               //    wrap            //    flex-start          //order         0
    //    column                    //    wrap-reverse    //    flex-end            //flex-grow     0
    //    column-reverse                                  //    center              //flex-shrink   1
    //                                                    //    space-between       //flex-basis    auto
    //  justify-content             //  align-items       //    space-around        //align-self
    //    ^flex-start               //    ^stretch                                  //    ^auto
    //    flex-end                  //    flex-start      //flex:1                  //    flex-start
    //    center                    //    flex-end                                  //    flex-end
    //    space-between             //    center                                    //    center
    //    space-around              //    baseline                                  //    baseline
//    https://scotch.io/tutorials/a-visual-guide-to-css3-flexbox-properties

    function randInt(min, max) {
        return min + Math.floor(Math.random() * (max - min + 1));
    }
    $(function () {
        $('[data-toggle="popover"]').popover()
    });
</script>
</body>
</html>


