<!DOCTYPE html><html lang="en"><head><meta charset="utf-8"><title>VI-xmlParser</title><meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link href="src/bootstrap/css/bootstrap.darkly.min.css" rel="stylesheet">
    <link href="src/css/styles.css" rel="stylesheet">
</head>

<body>
<div class="container-fluid">
    <div id="Header" class="row">
        <div class="" id="viheader" style="-moz-user-select: none; -webkit-user-select: none;
         -ms-user-select:none; user-select:none;-o-user-select:none;"
             unselectable="on" onselectstart="return false;" onmousedown="return false;">
            Vi XML Parser
        </div>
    </div>
    <div id="Input" class="row">
        <form method='get'>
            <div class="col-xs-12">
                <div class="input-group">
                    <div class="input-group-btn">
                        <button tabindex="-1" id="readXML" type="submit" class="btn btn-primary" name="readXML">Read XML</button>
                        <button tabindex="-1" data-toggle="dropdown" class="btn btn-default dropdown-toggle" type="button">
                            <span class="caret"></span>
                            <span class="sr-only">Toggle Dropdown</span>
                        </button>
                        <ul class="dropdown-menu" id="locationselector">
<!--                            <li><button type="button" class="locselect btn btn-primary btn-xs btn-block f-nova"
                                        value="http://campusdining.compass-usa.com/uncg/Pages/SignageXML.aspx?location=Fountain%20View">
                                    Fountain View</button></li>
                            <li><button type="button" class="locselect btn btn-primary btn-xs btn-block f-nova"
                                        value="http://morrisondining.compass-usa.com/sentara/Pages/SignageXML.aspx?location=Mountain%20View%20Cafe">
                                    Mountain View Cafe</button></li>
-->
<?php
$rawlog = file_get_contents('xmlparser_urls.json');
$decoded = json_decode($rawlog, true);
//    echo "<br/>>>decoded<<<br/>";print_r( $decoded );echo "<br/>>>END:decoded<<<br/>";
$itemcount = count($decoded);
//    echo "<br/>>>itemcount<<<br/>";print_r( $itemcount );echo "<br/>>>END:itemcount<<<br/>";
/** SORT BEFORE FOREACH */
//$decoded = array_multisort($decoded, SORT_ASC);
foreach ($decoded as $key => $val) {
    $btnloc = $key;
    $btnbrand = $decoded[$key]["brand"];
    $btnorg = $decoded[$key]["org"];
    $btnurl = $decoded[$key]["url"];
//        echo "<br/>>>btnurl<<<br/>";print_r( $btnurl );echo "<br/>>>END:btnurl<<<br/>";
    echo <<<EOT
    <li>
        <button type="button" class="locselect btn btn-primary btn-xs btn-block f-nova" value="$btnurl">
            ( $btnbrand )  <span style="font-weight:900;font-size:14px;">$btnloc</span>  ( $btnorg  )
        </button>
    </li>
EOT;
}
?>
                            <li class="divider"></li>
                        </ul>
                    </div>
                    <input id="inputarea" type="text" class="form-control" name="pastedURL"
                           onchange="checkinputarea();" onkeyup="this.onchange();"
                           onpaste="this.onchange();" oninput="this.onchange();"
                           placeholder="...XML address..." />
                </div>
            </div>
        </form>
    </div>
    <div id="NinjaOutput" class="row">
    <?php
if(isset($_GET['readXML'])) {
    $simpxml = simplexml_load_file($_GET['pastedURL'], 'SimpleXMLElement', LIBXML_NOWARNING);
    //$xml = simplexml_load_file('http://campusdining.compass-usa.com/uncg/Pages/SignageXML.aspx?location=Fountain%20View');
    $pasted_url = $_GET['pastedURL'];
    if (!$simpxml) {//error message
      echo "<div class='col-xs-8 col-xs-offset-2 text-center alert alert-info' style='opacity:.8;' role='alert'>...Camelot! ...Camelot~!  ...CAMELOT!!<br/>(it's only an error...) <br/>Check out the URL:</div>";
      echo "<div class='col-xs-8 col-xs-offset-2 text-center alert alert-info' id='successlinkdiv'><a href='".$pasted_url."' id='successlink'>$pasted_url</a></div>";
    } else {

$button_saveloc = <<<EOT
    <div class='col-sm-12'>
        <div id='successlinkdiv'><a href='$pasted_url' id='successlink'>$pasted_url</a></div>
        <button class="btn btn-success btn-xs" id="locsave" role="button">
            <span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
        </button>
        <span class="panel panel-success" style="display:none; color:#00ffbd;" id="locsave_response"></span>
    </div>
EOT;
        echo $button_saveloc;
        $domx = new DOMDocument("1.0");
        $domx->preserveWhiteSpace = false;
        $domx->formatOutput = true;
        $domx->loadXML($simpxml->asXML()); //domx is now xml object
        $domxText = print_r($domx, true);
        $formatted =  "<pre id='pretag' class='hide'>" . htmlentities($domx->saveXML()) . "</pre>";
        echo $formatted;
    }
}
    ?>
    </div>
    <div id="Tree">

    </div>
    <div id="JsOutput" class="row">
    </div>


</div>
<!--/////end:(BODY)/////-->

<br/><br/><br/>

<!-- ==========JS=========== -->
<!--##JQUERY-->
<script src="src/js/jquery-1.11.3.min.js" type="text/javascript"></script>
<!--##BOOTSTRAP_3.3-->
<script src="src/bootstrap/js/bootstrap.min.js"></script>
<!--##USER(JS)-->
<script src="src/js/script.js"></script>

</body>
</html>